#!/usr/bin/env python
# -*- coding: utf-8 -*-


from argparse import ArgumentParser
from javaexec.exec import JExec
from javaexec.parser import DirectoryCL, EclipseCL


def main(args):
    classpath = args.parser(args.ClassPath)

    prog = JExec(args.Command[0], classpath=classpath, args=args.Command[1:], jar=args.jar)
    prog.exec()


def parse_argv():
    parser = ArgumentParser(description="Determine the classpath and launch a java program.")

    parser.add_argument("Command", type=str, help="Main class to launch.", nargs='+')

    parser.add_argument(
        "-j",
        "--jar",
        type=str,
        default=None,
        help="Jar file to give to the '-jar' java option."
    )

    exclusive = parser.add_mutually_exclusive_group(required=True)

    exclusive.add_argument(
        "-c",
        "--classpath",
        dest='cl',
        action='append',
        help="List of directory to search jar and class files, or directly some files."
    )
    exclusive.add_argument(
        "--eclipse",
        dest='eclipse',
        nargs=1,
        help="Use the eclipse classpath xml file given on the cli, instead of a list of directory/files."
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_argv()

    # Dealing with the given argument:
    if args.eclipse is None:
        args.parser    = DirectoryCL
        args.ClassPath = args.cl
    else:
        args.parser    = EclipseCL
        args.ClassPath = args.eclipse

    main(
        args
    )

#  vim:set et sw=4 ts=4 ft=python:
