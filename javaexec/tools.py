#!/usr/bin/env python
# -*- coding: utf-8 -*-


from os.path import isdir, exists
from pathlib import Path
from zipfile import ZipFile


__all__ = [
    "check_present",
]


def _find(d, motif):
    d = Path(d)

    return d.rglob(motif)


def _is_ok(f):
    for l in f.readlines():
        if "main".encode() in l:
            return True


def check_present(main_class, classpath):
    """Check if `main_class` is present in the list of jar file `classpath`.

    :param main_class: Name of the main class to execute.
    :type: str
    :param classpath: Classpath to use.
    :type: list or tuple

    :return: True if found, False else.
    """
    if classpath is None or len(classpath) == 0:
        return False

    fname = main_class.replace(".", "/") + '.class'

    for cl in classpath:
        if not exists(cl):
            continue
        elif isdir(cl):
            for f in _find(cl, "*.class"):
                if fname == str(f.relative_to(cl)):
                    with f.open("rb") as to_check:
                        if _is_ok(to_check):
                            return True
        else:
            with ZipFile(cl) as fzip:
                if fname not in fzip.namelist():
                    continue

                with fzip.open(fname) as f:
                    if _is_ok(f):
                        return True

    return False


def complete(pattern, classpath):
    """Check if `main_class` is present in the list of jar file `classpath`.

    :param main_class: Name of the main class to execute.
    :type: str
    :param classpath: Classpath to use.
    :type: list or tuple

    :return: True if found, False else.
    """
    fname = pattern.replace(".", "/")

    results = list()

    for cl in classpath:
        if not exists(cl):
            continue
        elif isdir(cl):
            for f in _find(cl, "*.class"):
                if pattern in str(f.relative_to(cl)):
                    results.append(
                        str(f.relative_to(cl)).replace(".class", "").replace("/", ".")
                    )
        else:
            with ZipFile(cl) as fzip:
                for f in fzip.namelist():
                    if fname in f:
                        results.append(f.replace(".class", "").replace("/", "."))

    return results
