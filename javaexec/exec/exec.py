#!/usr/bin/env python
# -*- coding: utf-8 -*-


from os import execvp
from ..tools import check_present


# a command to run on the list of class files:
# find . -iname "*.class" -print | xargs sh -c 'javap -p "$@" | grep " main(" >/dev/null 2>/dev/null && printf "%s\n" "$@"'

class JavaClassNotFound(Exception):
    """Exception launched when the main class is not found."""
    pass


class JExec(object):
    """This class will determine all the main class and run the one asked by the user."""
    def __init__(self, main_class, classpath=['.'], jar=None, args=None):
        """Constructor.
        :param main_class: Name of the main class to call.
        :type main_class: str
        :param classpath: List of jar to use as a classpath.
        :type classpath: javaexec.parser.ClassPath
        :param args: Argument to give to the java program.
        :type args: list or tuple or None
        :param jar: Jar file to execute with the '-jar' option.
        :type jar: str or None
        """
        self._main_class = main_class
        self._classpath  = classpath
        self._args       = args
        self._jar        = jar
        self._exe        = 'java'

        if self._jar is None:
            self._check()

    def _check(self):
        found = False

        if check_present(self._main_class, self._classpath.jar):
            found = True

        if self._jar is not None:
            if check_present(self._main_class, [self._jar]):
                found = True

        if not found:
            raise JavaClassNotFound("The class '%s' is absent of the classpath or does not have a main method." % self._main_class)

    def _building_cli(self):
        cli = [ self._exe, '-classpath', str(self._classpath) ]

        if self._jar is not None:
            cli += [ '-jar', self._jar ]

        cli += [ self._main_class ]
        if self._args is not None:
            cli += self._args

        return cli

    def exec(self):
        execvp(
            self._exe,
            self._building_cli()
        )

    def __str__(self):
        return "%s %s" % ('java', " ".join(self._building_cli()))

    def __repr__(self):
        return self.__str__()
