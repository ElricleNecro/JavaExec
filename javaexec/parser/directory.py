#!/usr/bin/env python
# -*- coding: utf-8 -*-


from re import match
from pathlib import Path
from .classpath import ClassPath


def _find(dir, motif= ".*\.(jar|class)$"):
    cl = ClassPath()

    for d in dir.iterdir():
        if d.is_dir():
            cl += _find(d)
        else:
            if match(motif, d.name):
                cl += str(d)

    return cl


def from_dir(dir, motif= ".*\.(jar|class)$"):
    if not isinstance(dir, (list, tuple)):
        dir = [ dir ]

    cl = ClassPath()

    for d in dir:
        d = Path(d)

        if d.is_dir():
            cl += _find(d, motif)
        elif d.is_file():
            cl += str(d)

    return cl


class FromDir(ClassPath):
    """Create the classpath from a list of directory."""

    _motif = ".*\.(jar|class)$"

    def __init__(self, dir):
        super(FromDir, self).__init__()
        if not isinstance(dir, (list, tuple)):
            dir = [ dir ]

        for d in dir:
            d = Path(d)

            if d.is_dir():
                self._find(d)
            elif d.is_file():
                self.jar = str(d)

    def _find(self, dir):
        for d in dir.iterdir():
            if d.is_dir():
                self._find(d)
            else:
                if match(self._motif, d.name):
                    self.jar = str(d)
