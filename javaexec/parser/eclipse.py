#!/usr/bin/env python
# -*- coding: utf-8 -*-


from .classpath import ClassPath
from xml.etree import ElementTree as ET


def from_eclipse(fich, kind=["lib", "output"]):
    cl = ClassPath()

    if isinstance(fich, (tuple, list)):
        fich = fich[0]

    tree = ET.parse(fich)

    for el in tree.findall("classpathentry"):
        if el.attrib["kind"] in kind:
            cl += el.attrib["path"]

    return cl


class XMLEclipse(ClassPath):
    """Parse the classpath from eclipse '.classpath' file."""

    _kind = ["lib", "output"]

    def __init__(self, fich):
        super(XMLEclipse, self).__init__()

        if isinstance(fich, (tuple, list)):
            fich = fich[0]

        tree = ET.parse(fich)

        for el in tree.findall("classpathentry"):
            if el.attrib["kind"] in self._kind:
                self.jar = el.attrib["path"]
