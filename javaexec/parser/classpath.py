#!/usr/bin/env python
# -*- coding: utf-8 -*-


class ClassPath(object):
    """This class will deal with jar name, avoid multiplicity and return it formated."""
    def __init__(self):
        self._jarLst = list()

    @property
    def jar(self):
        """Get the jar list"""
        return self._jarLst

    @jar.setter
    def jar(self, new):
        if new not in self._jarLst:
            self._jarLst.append(new)

    def __str__(self):
        return ":".join(self._jarLst)

    def __repr__(self):
        return self.__str__()

    def __iadd__(self, val):
        if isinstance(val, ClassPath):
            self._jarLst += val.jar
        elif isinstance(val, (tuple, list)):
            for j in val:
                self._jarLst.append(j)
        elif isinstance(val, str):
            self._jarLst.append(str)

    def __add__(self, val):
        out = ClassPath()
        out._jarLst = self.jar.copy()

        if isinstance(val, ClassPath):
            out._jarLst += val.jar
        elif isinstance(val, (tuple, list)):
            for j in val:
                out._jarLst.append(j)
        elif isinstance(val, str):
            out._jarLst.append(str)

        return out

    def __radd__(self, val):
        return self + val
