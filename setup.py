#! /usr/bin/env python3
# -*- coding:Utf8 -*-

# -------------------------------------------------------------------------------------------------------------
# All necessary import:
# -------------------------------------------------------------------------------------------------------------
import glob
import setuptools as st

from distutils.core import setup
from distutils.command.install_data import install_data


def test_dependancy(*modules_list):
    import imp
    for deps in modules_list:
        try:
            imp.find_module(deps)
        except ImportError:
            print("Module %s not found." % deps)


packages = st.find_packages()

# -------------------------------------------------------------------------------------------------------------
# Call the setup function:
# -------------------------------------------------------------------------------------------------------------
setup(
    name='javaexec',
    version='0.1',
    description='Python Module to execute Java program with a peculiar classpath.',
    author='Guillaume Plum',
    packages=packages,
    cmdclass={'install_data': install_data},
    scripts=glob.glob("scripts/*"),
)

#  data_files  = [
#  ('share/LibThese/animation-plugins',
#  ["share/LibThese/animation-plugins/__init__.py"]),
# glob.glob("share/LibThese/animation-plugins/*.py")),
#  ('share/LibThese/', ["share/LibThese/config.yml",
#  "share/LibThese/filter.yml"]),
# glob.glob("share/LibThese/animation-plugins/*.py")),
#  ],
#  scripts = [
#  'scripts/animationv2.py',
#  'scripts/models_plot.py',
#  'scripts/roi.py',
#  'scripts/verif_python.py',
#  ],

# vim:spelllang=
